import cv2
import numpy as np
import os
from random import shuffle
from tqdm import tqdm


TRAIN_DIR = 'C:/Dev/machine learning projects/dogs-and-cats-with-tf/all/train'
TEST_DIR = 'C:/Dev/machine learning projects/dogs-and-cats-with-tf/all/test'
IMG_SIZE = 256
LR = 1e-3
sigma= 0.33

MODEL_NAME = "dogsvscats-{}-{}.model".format(LR,"50.5.1.11conv-basic")


def label_img(img):
    word_label = img.split('.')[-3]
    if word_label == 'cat': return [1,0]
    elif word_label == 'dog': return [0,1]
    
    
def create_train_data():
    training_data = []
    for img in tqdm(os.listdir(TRAIN_DIR)):
        label = label_img(img)
        path = os.path.join(TRAIN_DIR, img)
        img = cv2.resize(cv2.GaussianBlur(cv2.imread(path, cv2.IMREAD_GRAYSCALE),(5,5),0), (IMG_SIZE,IMG_SIZE))
        v = np.median(img)
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        img = cv2.Canny(img,lower,upper)
        training_data.append([np.array(img),np.array(label)])
        
    shuffle(training_data)
    np.save("train_data_gauss_canny_fin6.npy", training_data)
    return training_data
        

def process_test_data():
    testing_data = []
    for img in tqdm(os.listdir(TEST_DIR)):
        path = os.path.join(TEST_DIR, img)
        img_num = img.split('.')[0]
        img = cv2.resize(cv2.GaussianBlur(cv2.imread(path, cv2.IMREAD_GRAYSCALE),(5,5),0), (IMG_SIZE,IMG_SIZE))
        v = np.median(img)
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        img = cv2.Canny(img,lower,upper)
        testing_data.append([np.array(img), img_num])
        
    np.save("test_data_gauss_canny_fin6.npy",testing_data)
    return testing_data

       

train_data = create_train_data()
    

import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
import tensorflow as tf

tf.reset_default_graph()

convnet = input_data(shape=[None, IMG_SIZE, IMG_SIZE, 1], name='input')

convnet = conv_2d(convnet, 32, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = conv_2d(convnet, 64, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = conv_2d(convnet, 128, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = conv_2d(convnet, 64, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = fully_connected(convnet, 2056, activation='relu')
convnet = dropout(convnet, 0.8)


convnet = fully_connected(convnet, 2, activation='softmax')
convnet = regression(convnet, optimizer='adam', learning_rate=LR, loss='categorical_crossentropy', name='targets')

model = tflearn.DNN(convnet, tensorboard_dir='log')



if os.path.exists('{}.meta'.format(MODEL_NAME)):
    model.load(MODEL_NAME)
    print('model loaded!')
    
    
train = train_data[:-500]
test = train_data[-500:]

X=np.array([i[0]for i in train]).reshape(-1,IMG_SIZE,IMG_SIZE,1)
Y=[i[1]for i in train]




test_x =np.array([i[0]for i in test]).reshape(-1,IMG_SIZE,IMG_SIZE,1)
test_y =[i[1]for i in test]

model.fit({'input': X}, {'targets': Y}, n_epoch=5,validation_set=({'input': test_x}, {'targets': test_y}), snapshot_step=500, show_metric=True, run_id=MODEL_NAME)
    
model.save(MODEL_NAME)

model.load()


import matplotlib.pyplot as plt

test_data = process_test_data()

#test_data = np.load("test_data.npy")

fig = plt.figure()

for num, data in enumerate(test_data[:12]):
    
    img_num = data[1]
    img_data = data[0]
    
    y = fig.add_subplot(3,4,num+1)
    orig = img_data
    data = img_data.reshape(IMG_SIZE,IMG_SIZE,1)
    
# =============================================================================
#     model_out = model.predict([data])[0]
#     
#     if np.argmax(model_out) == 1: str_lbl = "DOG"
#     else: str_lbl = "CAT"
# =============================================================================
    
    y.imshow(orig, cmap='gray')
#    plt.title(str_lbl)
    
    y.axes.get_xaxis().set_visible(False)
    y.axes.get_yaxis().set_visible(False)
    
plt.show()
    

with open('submission_file_gauss_canny1.csv','w') as f:
    f.write('id,label\n')
            
with open('submission_file_gauss_canny2.csv','a') as f:
    for data in tqdm(test_data):
        img_num = data[1]
        img_data = data[0]
        orig = img_data
        data = img_data.reshape(IMG_SIZE,IMG_SIZE,1)
        model_out = model.predict([data])[0]
        f.write('{},{}\n'.format(img_num,model_out[1]))